<?php
header('Access-Control-Allow-Origin: http://localhost:8080');
header('Access-Control-Allow-Max-Age: 3600');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');

// Check if request method is OPTIONS
if (strtoupper($_SERVER['REQUEST_METHOD']) === 'OPTIONS') {
	// Check if HTTP_ACCESS_CONTROL_REQUEST_METHOD is set
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
		// Set Access-Control-Allow-Methods header
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
	}

	// Check if HTTP_ACCESS_CONTROL_REQUEST_HEADERS is set
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
		// Set Access-Control-Allow-Headers header
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	}

	//
	exit(0);
}

?>