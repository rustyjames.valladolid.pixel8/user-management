<?php

spl_autoload_register("autoload");

function autoload($className) {

	$url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	if (strpos($url, "actions") !== false) {

		$path = "../classes/";
	} else {
		$path = "classes/";
	}

	$path = $path . $className . ".class.php";

	if (!file_exists($path)) {
		return false;
	}
	require_once $path;
}
