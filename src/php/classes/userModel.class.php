<?php

class UserModel extends DbConnection {
	// get users
	protected function selectUsers() {
		global $stmt;
		$sql = "SELECT * FROM users";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();
	}

	// get specific user
	protected function selectSpecificUser($userId) {
		global $stmt;
		$sql = "SELECT * FROM users WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}

	// create user account
	protected function insertUserAccount($username, $password, $age, $email, $birthDate, $level) {
		$sql = "INSERT INTO users (USERNAME,PASSWORD,AGE,EMAIL,BIRTHDATE,USER_LEVEL) VALUES (?,?,?,?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$username, $password, $age, $email, $birthDate, $level]);
	}
	// update user
	protected function updateUserAccount($age, $email, $birthDate, $userId) {
		$sql = "UPDATE users SET AGE = ?, EMAIL = ?, BIRTHDATE = ? WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$age, $email, $birthDate, $userId]);
	}
	//  delete user
	protected function deleteUserAccount($userId) {
		$sql = "DELETE FROM users WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}
	// check username if already exist
	protected function checkUsernameDuplicate($username) {
		$sql = "SELECT USERNAME AS USERNAME FROM users WHERE USERNAME = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$username]);
		while ($row = $stmt->fetch()) {
			return $row['USERNAME'];
		}
	}
	// add to do
	protected function setToDo($userId, $notes, $status) {
		$sql = "INSERT INTO todo (USER_ID,NOTES) VALUES (?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId, $notes, $status]);
	}
	// validate user
	protected function selectUserAccount($username) {
		$sql = "SELECT * FROM users WHERE USERNAME = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$username]);
		global $encryptedPwd;
		global $level;
		while ($row = $stmt->fetch()) {
			$encryptedPwd = $row['PASSWORD'];
			$level = $row['USER_LEVEL'];
		}
	}
	// get user id for setting session id
	protected function selectUserId($username) {
		$sql = "SELECT USER_ID AS USER_ID FROM users WHERE USERNAME = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$username]);
		$row = $stmt->fetch();
		return $row['USER_ID'];
	}
	protected function removeAccount($userId) {
		$sql = "DELETE FROM users WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}
	// change password
	protected function editPassword($newPwd, $userId) {
		$sql = "UPDATE users SET PASSWORD = ? WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$newPwd, $userId]);
	}

	protected function selectUserMng($userId) {
		global $stmt;
		$sql = "SELECT * FROM users WHERE USER_LEVEL != 'admin' AND USER_ID != ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}

	protected function selectUserInfo() {
		global $stmt;
		$sql = "SELECT * FROM users WHERE USER_LEVEL != 'admin'";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();
	}

}
