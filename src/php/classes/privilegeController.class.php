<?php

class PrivilegeController extends PrivilegeModel {
	// check if user already exist in user management privilege table
	public function getUserId($table, $userId) {
		return $this->selectUserId($table, $userId);
	}
	// user management privilege ADD
	public function setUserMngmntPriv($userId, $access, $modify) {
		$this->insertUserMngmntPriv($userId, $access, $modify);
	}
	// user management privilege UPDATE
	public function editUserMngmntPriv($access, $modify, $userId) {
		$this->updateUserMngmntPriv($access, $modify, $userId);
	}
	// user management privilege GET
	public function getUserMngmntPriv($userId) {
		$this->selectUserMngmntPriv($userId);
	}
	// user information privilege ADD
	public function setUserInfoPriv($userId, $access, $add, $edit, $delete) {
		$this->insertUserInfoPriv($userId, $access, $add, $edit, $delete);
	}
	// user information privilege UPDATE
	public function editUserInfoPriv($access, $add, $edit, $delete, $userId) {
		$this->updateUserInfoPriv($access, $add, $edit, $delete, $userId);
	}
	// user information privilege GET
	public function getUserInfoPriv($userId) {
		$this->selectUserInfoPriv($userId);
	}
	// todo privilege
	public function setTodoPriv($userId, $access, $add, $edit, $delete) {
		$this->insertTodoPriv($userId, $access, $add, $edit, $delete);
	}
	// todo privilege UPDATE
	public function editTodoPriv($access, $add, $edit, $delete, $userId) {
		$this->updateTodoPriv($access, $add, $edit, $delete, $userId);
	}
	// todo privilege GET
	public function getTodoPriv($userId) {
		$this->selectTodoPriv($userId);
	}
}