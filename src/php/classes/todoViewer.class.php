<?php

class TodoViewer extends TodoModel {
	// get todos
	public function selectAllToDos() {
		$this->getAllToDos();
	}
	// get todos for specific user
	public function selectToDos($userId) {
		$this->getToDos($userId);
	}
}
