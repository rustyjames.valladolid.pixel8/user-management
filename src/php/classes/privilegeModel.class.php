<?php

class PrivilegeModel extends DbConnection {
	// check if user already exist in user management privilege table
	protected function selectUserId($table, $userId) {
		$sql = "SELECT USER_ID AS USER_ID FROM $table WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);

		while ($row = $stmt->fetch()) {
			return $row['USER_ID'];
		}
	}
	// user management privilege ADD
	protected function insertUserMngmntPriv($userId, $access, $modify) {
		$sql = "INSERT INTO user_management_priv (USER_ID,ACCESS_USER,MODIFY_USER) VALUES (?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId, $access, $modify]);
	}

	// user management privilege UPDATE
	protected function updateUserMngmntPriv($access, $modify, $userId) {
		$sql = "UPDATE user_management_priv SET ACCESS_USER = ?, MODIFY_USER = ? WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$access, $modify, $userId]);
	}
	// user management privilege GET
	protected function selectUserMngmntPriv($userId) {
		global $stmt;
		$sql = "SELECT * FROM user_management_priv WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}

	// user information privilege ADD
	protected function insertUserInfoPriv($userId, $access, $add, $edit, $delete) {
		$sql = "INSERT INTO user_information_priv (USER_ID,ACCESS_USER,ADD_USER,EDIT_USER,DELETE_USER) VALUES (?,?,?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId, $access, $add, $edit, $delete]);
	}
	// user information privilege UPDATE
	protected function updateUserInfoPriv($access, $add, $edit, $delete, $userId) {
		$sql = "UPDATE user_information_priv SET ACCESS_USER = ?, ADD_USER = ?, EDIT_USER = ?, DELETE_USER = ? WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$access, $add, $edit, $delete, $userId]);
	}
	// user information privilege GET
	protected function selectUserInfoPriv($userId) {
		global $stmt;
		$sql = "SELECT * FROM user_information_priv WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);

	}
	// todo privilege ADD
	protected function insertTodoPriv($userId, $access, $add, $edit, $delete) {
		$sql = "INSERT INTO todo_priv (USER_ID,ACCESS_USER,ADD_USER,EDIT_USER,DELETE_USER) VALUES (?,?,?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId, $access, $add, $edit, $delete]);
	}
	// todo privilege UPDATE
	protected function updateTodoPriv($access, $add, $edit, $delete, $userId) {
		$sql = "UPDATE todo_priv SET ACCESS_USER = ?, ADD_USER = ?, EDIT_USER = ?, DELETE_USER = ? WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$access, $add, $edit, $delete, $userId]);
	}
	// todo privilege GET
	protected function selectTodoPriv($userId) {
		global $stmt;
		$sql = "SELECT * FROM todo_priv WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}
}