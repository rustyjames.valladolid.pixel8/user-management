<?php

class ToDoModel extends DbConnection {
	// add todo
	protected function setTask($userId, $notes, $status) {
		$sql = "INSERT INTO todo (USER_ID,NOTES,STATUS) VALUES (?,?,?)";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId, $notes, $status]);
	}

	// get all todos
	protected function getAllToDos() {
		global $stmt;
		$sql = "SELECT TODO_ID,todo.USER_ID,USERNAME,NOTES,STATUS FROM todo INNER JOIN users ON todo.USER_ID = users.USER_ID";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();
	}

	// get todos for specific user
	protected function getToDos($userId) {
		global $stmt;
		$sql = "SELECT * FROM todo WHERE USER_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$userId]);
	}

	// update todo
	protected function updateToDo($notes, $status, $todoId) {
		$sql = "UPDATE todo SET NOTES = ?, STATUS = ? WHERE TODO_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$notes, $status, $todoId]);
	}

	// delete todo
	protected function deleteToDo($todoId) {
		$sql = "DELETE FROM todo WHERE TODO_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$todoId]);
	}

	// update todo status
	protected function updateToDoStat($status, $todoId) {
		$sql = "UPDATE todo SET STATUS = ? WHERE TODO_ID = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$status, $todoId]);
	}
}
