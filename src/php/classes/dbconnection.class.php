<?php

class DbConnection {

	private $host = "localhost";
	private $username = "root";
	private $password = "";
	private $dbName = "test";

	protected function connect() {

		try {
			$dsn = "mysql:host=" . $this->host . ";dbname=" . $this->dbName;
			$pdo = new PDO($dsn, $this->username, $this->password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
}
