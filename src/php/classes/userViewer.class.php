<?php

class UserViewer extends UserModel {
	// get users
	public function getUserProfile() {
		$this->selectUsers();
	}

	// get specific user
	public function getSpecificUser($userId) {
		$this->selectSpecificUser($userId);
	}

	// get user mng list
	public function getUserMng($userId) {
		$this->selectUserMng($userId);
	}

	// get user info list
	public function getUserInfo() {
		$this->selectUserInfo();
	}
}
