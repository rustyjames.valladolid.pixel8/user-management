<?php

class TodoController extends TodoModel {
	// add todo
	public function inserTask($userId, $notes, $status) {
		$this->setTask($userId, $notes, $status);
	}

	// update todo
	public function editToDo($notes, $status, $todoId) {
		$this->updateToDo($notes, $status, $todoId);
	}

	// delete todo
	public function removeToDo($todoId) {
		$this->deleteToDo($todoId);
	}

	// update todos status
	public function editToDoStat($status, $todoId) {
		$this->updateToDoStat($status, $todoId);
	}
}
