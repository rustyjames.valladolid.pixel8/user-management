<?php

class UserController extends UserModel {
	// create user
	public function setUserAccount($username, $password, $age, $email, $birthDate, $level) {
		$this->insertUserAccount($username, $password, $age, $email, $birthDate, $level);
	}
	// update user account
	public function editUserAccount($age, $email, $birthDate, $userId) {
		$this->updateUserAccount($age, $email, $birthDate, $userId);
	}
	// check username if already exist
	public function checkUsername($username) {
		return $this->checkUsernameDuplicate($username);
	}
	// delete user
	public function deleteAccount($userId) {
		$this->removeAccount($userId);
	}
	// validate user access
	public function verifyUserAccount($username) {
		$this->selectUserAccount($username);
	}
	// get user id for setting session id
	public function getUserId($username) {
		return $this->selectUserId($username);
	}

	public function removeUserAccount($userId) {
		$this->deleteUserAccount($userId);
	}

	public function updatePassword($newPwd, $userId) {
		$this->editPassword($newPwd, $userId);
	}
}
