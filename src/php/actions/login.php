<?php

include '../header/header.php';
session_start();
include '../includes/autoload.inc.php';
$userC = new UserController;
$privilege = new PrivilegeController;

$received_data = json_decode(file_get_contents("php://input"));

// // REQUESTS

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	if (!isset($_SESSION['user-id'])) {
		$result = array('error' => true);

	} else {
		$result = array('error' => false);
	}

	echo json_encode($result);

}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$username = filter_var($received_data->username, FILTER_SANITIZE_STRING);
	$password = filter_var($received_data->password, FILTER_SANITIZE_STRING);

	//check username if exist in database
	$userC->verifyUserAccount($username);
	// $encryptedPwd variable is from verifyUserAccount($username) that sets to global
	$encryptedPwd = $encryptedPwd;
	// executes if username found
	if ($encryptedPwd) {
		// if username found get its password and match to the inputed password by user
		$verify = password_verify($password, $encryptedPwd);
		// executes if password matched
		if ($verify == 1) {

			$userId = $userC->getUserId($username);
			$privilege->getUserMngmntPriv($userId);
			$privilege->getUserInfoPriv($userId);
			$privilege->getTodoPriv($userId);
			$_SESSION['user-id'] = $userId;
			$_SESSION['user-level'] = $level;
			$result = array('title' => 'Success', 'message' => 'Login Successful! ', 'userId' => $userId, 'userLevel' => $level, 'umAccess' => $umAccess, 'umModify' => $umModify, 'uiAccess' => $uiAccess, 'uiAdd' => $uiAdd, 'uiEdit' => $uiEdit, 'uiDelete' => $uiDelete, 'tdAccess' => $tdAccess, 'tdAdd' => $tdAdd, 'tdEdit' => $tdEdit, 'tdDelete' => $tdDelete, 'error' => false);

			// executes if inputed password does not found to the database
		} else {
			$result = array('title' => 'Error', 'message' => 'Username and Password not found!', 'error' => true);
		}
		// executes if username not foud to the database
	} else {
		$result = array('title' => 'Error', 'message' => 'Username and Password not found!', 'error' => true);
	}
	echo json_encode($result);
}
