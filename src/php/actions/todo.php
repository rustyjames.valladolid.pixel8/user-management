<?php

header('Access-Control-Allow-Origin: *');

header('Content-Type: application/json');

// Check if request method is OPTIONS
if (strtoupper($_SERVER['REQUEST_METHOD']) === 'OPTIONS') {
	// Check if HTTP_ACCESS_CONTROL_REQUEST_METHOD is set
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
		// Set Access-Control-Allow-Methods header
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
	}

	// Check if HTTP_ACCESS_CONTROL_REQUEST_HEADERS is set
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
		// Set Access-Control-Allow-Headers header
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	}

	//
	exit(0);
}

include '../includes/autoload.inc.php';
$todoV = new ToDoViewer;
$todoC = new ToDoController;

$received_data = json_decode(file_get_contents("php://input"));

// REQUESTS
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	$data = array();
	$todoV->selectAllToDos();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$data[] = $row;
	}
	echo json_encode($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$userId = $received_data->userId;
	$note = $received_data->note;

	$todoC->inserTask($userId, $note, 'Pending');
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
	$todoId = $received_data->todoId;
	$note = $received_data->note;
	$status = $received_data->status;

	$todoC->editToDo($note, $status, $todoId);
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {

	$todoId = $received_data->todoId;
	$todoC->removeToDo($todoId);

}