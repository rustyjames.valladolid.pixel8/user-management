<?php

include '../header/header.php';
session_start();
include '../includes/autoload.inc.php';
$userV = new UserViewer;
$userC = new UserController;

$received_data = json_decode(file_get_contents("php://input"));
// REQUESTS
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	$data = array();
	$userId = $_SESSION['user-id'];
	$userLevel = $_SESSION['user-level'];
	if (isset($_GET['req'])) {
		if ($_GET['req'] == 'user-mng-fetch-all') {
			if ($userLevel != 'admin') {
				$userV->getUserMng($userId);
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$data[] = $row;
				}
			} else {
				$userV->getUserProfile();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$data[] = $row;
				}
			}

		} else if ($_GET['req'] == 'user-info-fetch-all') {
			if ($userLevel != 'admin') {
				$userV->getUserInfo();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$data[] = $row;
				}
			} else {
				$userV->getUserProfile();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$data[] = $row;
				}
			}
		} else if ($_GET['req'] == 'fetch-profile') {
//
			$userV->getSpecificUser($userId);
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$data[] = $row;
			}
		}
	} else {
		$userV->getUserProfile();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$data[] = $row;
		}
	}

	echo json_encode($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$email = filter_var($received_data->email, FILTER_SANITIZE_EMAIL);
	$age = filter_var($received_data->age, FILTER_SANITIZE_NUMBER_INT);
	$birthDate = filter_var($received_data->birthDate, FILTER_SANITIZE_STRING);
	$userName = filter_var($received_data->userName, FILTER_SANITIZE_STRING);
	$password = filter_var($received_data->password, FILTER_SANITIZE_STRING);
	$confirmPassword = $received_data->confirmPassword;
	$level = strtolower($received_data->level);

	// encrypt password
	$encPassword = password_hash($password, PASSWORD_DEFAULT);

	if ($userC->checkUsername($userName)) {
		$result = array('title' => 'Error', 'message' => 'Username already exist!', 'error' => true);
	} else if (strlen($password) < 8) {
		$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
	} else if (!preg_match("#[0-9]+#", $password)) {
		$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
	} else if (!preg_match("#[A-Z]+#", $password)) {
		$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
	} else if (!preg_match("#[a-z]+#", $password)) {
		$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
	} else if ($password != $confirmPassword) {
		$result = array('title' => 'Error', 'message' => 'Password not match!', 'error' => true);
	} else {
		$result = array('title' => 'Success', 'message' => 'User has been created', 'error' => false);
		$userC->setUserAccount($userName, $encPassword, $age, $email, $birthDate, $level);
	}

	echo json_encode($result);
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

	if ($received_data->action == 'edit') {
		$userId = filter_var($received_data->userId, FILTER_SANITIZE_NUMBER_INT);
		$email = filter_var($received_data->email, FILTER_SANITIZE_EMAIL);
		$age = filter_var($received_data->age, FILTER_SANITIZE_NUMBER_INT);
		$birthDate = filter_var($received_data->birthDate, FILTER_SANITIZE_STRING);
		$userC->editUserAccount($age, $email, $birthDate, $userId);
	}

	if ($received_data->action == 'change-pwd') {
		$userId = $received_data->userId;
		$newPwd = $received_data->newPwd;
		$confirmPwd = $received_data->confirmPwd;
		$newEncPwd = password_hash($newPwd, PASSWORD_DEFAULT);

		if (strlen($newPwd) < 8) {
			$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
		} else if (!preg_match("#[0-9]+#", $newPwd)) {
			$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
		} else if (!preg_match("#[A-Z]+#", $newPwd)) {
			$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
		} else if (!preg_match("#[a-z]+#", $newPwd)) {
			$result = array('title' => 'Error', 'message' => 'Password must have atleast 8 characters with a combination of lowercase and uppercase letters and number!', 'error' => true);
		} else if ($newPwd != $confirmPwd) {
			$result = array('title' => 'Error', 'message' => 'Password not match', 'error' => true);
		} else {
			$result = array('title' => 'Success', 'message' => 'Password has been changed', 'error' => false);
			$userC->updatePassword($newEncPwd, $userId);
		}
		echo json_encode($result);
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {

	$userId = $received_data->userId;
	$userC->deleteAccount($userId);

}