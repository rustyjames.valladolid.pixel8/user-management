<?php

include '../header/header.php';
include '../includes/autoload.inc.php';
$privilege = new PrivilegeController;

$received_data = json_decode(file_get_contents("php://input"));

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	$privilege->getUserInfoPriv($_GET['userId']);
	while ($row = $stmt->fetch()) {
		$uiAccess = $row['ACCESS_USER'];
		$uiAdd = $row['ADD_USER'];
		$uiEdit = $row['EDIT_USER'];
		$uiDelete = $row['DELETE_USER'];
	}
	$data = array('uiAccess' => $uiAccess, 'uiAdd' => $uiAdd, 'uiEdit' => $uiEdit, 'uiDelete' => $uiDelete);
	//
	echo json_encode($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$userId = $received_data->userId;
	$access = $received_data->access;
	$add = $received_data->add;
	$update = $received_data->update;
	$delete = $received_data->delete;

	if ($privilege->getUserId('user_information_priv', $userId)) {
		$privilege->editUserInfoPriv($access, $add, $update, $delete, $userId);
	} else {
		$privilege->setUserInfoPriv($userId, $access, $add, $update, $delete);
	}

	// $result = array('message' => $userId . ' ' . $access);
	// echo json_encode($result);
}
