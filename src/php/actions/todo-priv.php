<?php

include '../header/header.php';

include '../includes/autoload.inc.php';
$privilege = new PrivilegeController;

$received_data = json_decode(file_get_contents("php://input"));

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	$privilege->getTodoPriv($_GET['userId']);

	while ($row = $stmt->fetch()) {
		$tdAccess = $row['ACCESS_USER'];
		$tdAdd = $row['ADD_USER'];
		$tdEdit = $row['EDIT_USER'];
		$tdDelete = $row['DELETE_USER'];
	}
	$data = array('tdAccess' => $tdAccess, 'tdAdd' => $tdAdd, 'tdEdit' => $tdEdit, 'tdDelete' => $tdDelete);
	//
	echo json_encode($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$userId = $received_data->userId;
	$access = $received_data->access;
	$add = $received_data->add;
	$update = $received_data->update;
	$delete = $received_data->delete;

	if ($privilege->getUserId('todo_priv', $userId)) {
		$privilege->editTodoPriv($access, $add, $update, $delete, $userId);
	} else {
		$privilege->setTodoPriv($userId, $access, $add, $update, $delete);
	}

	// $result = array('message' => $userId . ' ' . $access);
	// echo json_encode($result);
}
