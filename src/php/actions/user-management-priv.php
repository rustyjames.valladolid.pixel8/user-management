<?php

include '../header/header.php';

include '../includes/autoload.inc.php';
$privilege = new PrivilegeController;

$received_data = json_decode(file_get_contents("php://input"));
// REQUESTS
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	$privilege->getUserMngmntPriv($_GET['userId']);
	while ($row = $stmt->fetch()) {
		$umAccess = $row['ACCESS_USER'];
		$umModify = $row['MODIFY_USER'];
	}
	$data = array('umAccess' => $umAccess, 'umModify' => $umModify);
	//
	echo json_encode($data);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$userId = $received_data->userId;
	$access = $received_data->access;
	$modify = $received_data->modify;
	if ($privilege->getUserId('user_management_priv', $userId)) {
		$privilege->editUserMngmntPriv($access, $modify, $userId);
	} else {
		$privilege->setUserMngmntPriv($userId, $access, $modify);
	}

}
