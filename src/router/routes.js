const routes = [
  {
    path: "/",
    component: () => import("layouts/Login.vue"),
  },
  {
    path: "/layout",
    component: () => import("layouts/Layout.vue"),
    children: [
      {
        path: "/home",
        component: () => import("pages/Home.vue"),
      },
      {
        path: "/user-management",
        component: () => import("pages/UserMngmnt.vue"),
      },
      {
        path: "/user-information",
        component: () => import("pages/UserInfo.vue"),
      },
      { path: "/todo-list", component: () => import("pages/Todo.vue") },
      { path: "/logout", component: () => import("pages/Logout.vue") },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
