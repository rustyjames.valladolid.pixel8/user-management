// user *****************************************
// modals/dialog
export function userEdit(state) {
	return state.userEdit;
}
export function userDel(state) {
	return state.userDel;
}
export function userAddTask(state) {
	return state.userAddTask;
}
export function userCreate(state) {
	return state.userCreate;
}
export function userChangePwd(state) {
	return state.userChangePwd;
}
export function userPriv(state) {
	return state.userPriv;
}
// user form data
export function userName(state) {
	return state.userName;
}
export function userEmail(state) {
	return state.userEmail;
}
export function userAge(state) {
	return state.userAge;
}
export function userBirthDate(state) {
	return state.userBirthDate;
}
export function userId(state) {
	return state.userId;
}
export function userLevel(state) {
	return state.userLevel;
}
//***********************************************

// todo *****************************************
export function taskEdit(state) {
	return state.taskEdit;
}
export function taskDel(state) {
	return state.taskDel;
}
export function taskAdd(state) {
	return state.taskAdd;
}
// todo form data
export function todoId(state) {
	return state.todoId;
}
export function todoNote(state) {
	return state.todoNote;
}
export function todoName(state) {
	return state.todoName;
}
export function todoStatus(state) {
	return state.todoStatus;
}
//***********************************************

// user management privilege ********************
export function umAccess(state) {
	return state.umAccess;
}
export function umModify(state) {
	return state.umModify;
}
//***********************************************

// user information privilege ********************
export function uiAccess(state) {
	return state.uiAccess;
}
export function uiAdd(state) {
	return state.uiAdd;
}
export function uiEdit(state) {
	return state.uiEdit;
}
export function uiDelete(state) {
	return state.uiDelete;
}
//***********************************************

// todo privilege ********************
export function tdAccess(state) {
	return state.tdAccess;
}
export function tdAdd(state) {
	return state.tdAdd;
}
export function tdEdit(state) {
	return state.tdEdit;
}
export function tdDelete(state) {
	return state.tdDelete;
}
//***********************************************
