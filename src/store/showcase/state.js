export default function () {
	return {
		// user modals
		userEdit: false,
		userDel: false,
		userAddTask: false,
		userCreate: false,
		userChangePwd: false,
		userPriv: false,

		// user form
		userName: null,
		userEmail: null,
		userAge: null,
		userBirthDate: null,
		userId: null,
		userLevel: null,

		// todo modals
		taskEdit: false,
		taskDel: false,
		taskAdd: false,

		// todo form
		todoNote: null,
		todoName: null,
		todoStatus: null,
		todoId: null,

		// privilege
		//user management
		umAccess: null,
		umModify: null,
		//user information
		uiAccess: null,
		uiAdd: null,
		uiEdit: null,
		uiDelete: null,
		// todo privilege
		tdAccess: null,
		tdAdd: null,
		tdEdit: null,
		tdDelete: null,
	};
}
