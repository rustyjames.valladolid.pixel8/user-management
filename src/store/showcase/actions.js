// user **********************************************
// show/hide modal user edit form
export const userEdit = ({ commit }, payload) => {
	commit("userEdit", payload);
};
// show/hide modal user delete form
export const userDel = ({ commit }, payload) => {
	commit("userDel", payload);
};
// show/hide modal user delete form
export const userAddTask = ({ commit }, payload) => {
	commit("userAddTask", payload);
};
export const userCreate = ({ commit }, payload) => {
	commit("userCreate", payload);
};
export const userChangePwd = ({ commit }, payload) => {
	commit("userChangePwd", payload);
};
export const userPriv = ({ commit }, payload) => {
	commit("userPriv", payload);
};
// user form data
export const userName = ({ commit }, payload) => {
	commit("userName", payload);
};
export const userEmail = ({ commit }, payload) => {
	commit("userEmail", payload);
};
export const userAge = ({ commit }, payload) => {
	commit("userAge", payload);
};
export const userBirthDate = ({ commit }, payload) => {
	commit("userBirthDate", payload);
};
export const userId = ({ commit }, payload) => {
	commit("userId", payload);
};
export const userLevel = ({ commit }, payload) => {
	commit("userLevel", payload);
};

// user modals/dialog
export const hideUserEditForm = (state, payload) => {
	commit("hideUserEditForm", payload);
};
export const hideUserDelDialog = (state, payload) => {
	commit("hideUserDelDialog", payload);
};
export const hideUserAddForm = (state, payload) => {
	commit("hideUserAddForm", payload);
};
export const hideUserCreateForm = (state, payload) => {
	commit("hideUserCreateForm", payload);
};
export const hideUserChangePwdForm = (state, payload) => {
	commit("hideUserChangePwdForm", payload);
};
export const hideUserPrivPwdForm = (state, payload) => {
	commit("hideUserPrivPwdForm", payload);
};
//***************************************************

// todo **********************************************
// show/hide modal  todo edit form
export const taskEdit = ({ commit }, payload) => {
	commit("taskEdit", payload);
};
export const taskDel = ({ commit }, payload) => {
	commit("taskDel", payload);
};
export const taskAdd = ({ commit }, payload) => {
	commit("taskAdd", payload);
};
// todo form data
export const taskNote = ({ commit }, payload) => {
	commit("taskNote", payload);
};
export const todoStatus = ({ commit }, payload) => {
	commit("todoStatus", payload);
};
// todo modals/dialog
export const hideEditForm = (state, payload) => {
	commit("hideEditForm", payload);
};
export const hideDelDialog = (state, payload) => {
	commit("hideDelDialog", payload);
};
export const hideAddDialog = (state, payload) => {
	commit("hideAddDialog", payload);
};
//***************************************************

//user management priv ******************************
export const userManagementPriv = ({ commit }, payload) => {
	commit("userManagementPriv", payload);
};
//***************************************************

//user information priv ******************************
export const userInformationPriv = ({ commit }, payload) => {
	commit("userInformationPriv", payload);
};
//****************************************************

//todo priv ******************************************
export const todoPriv = ({ commit }, payload) => {
	commit("todoPriv", payload);
};
//****************************************************
