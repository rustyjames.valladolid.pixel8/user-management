// user data ********************************************
export const userEdit = (state, payload) => {
	state.userId = payload.userId;
	state.userName = payload.userName;
	state.userEmail = payload.userEmail;
	state.userAge = payload.userAge;
	state.userBirthDate = payload.userBirthDate;
	state.userEdit = payload.editForm;
};
export const userDel = (state, payload) => {
	state.userId = payload.userId;
	state.userName = payload.userName;
	state.userDel = payload.delDialog;
};
export const userAddTask = (state, payload) => {
	state.userId = payload.userId;
	state.userName = payload.userName;
	state.userAddTask = payload.addForm;
	state.taskAdd = payload.addTask;
};
export const userCreate = (state, payload) => {
	state.userCreate = payload;
};
export const userChangePwd = (state, payload) => {
	state.userId = payload.userId;
	state.userName = payload.userName;
	state.userChangePwd = payload.changePwdForm;
};
export const userPriv = (state, payload) => {
	state.userId = payload.userId;
	state.userName = payload.userName;
	state.userPriv = payload.userPriv;
};

// // modals/dialog ********************************************
export const hideUserEditForm = (state, payload) => {
	state.userEdit = payload;
};
export const hideUserDelDialog = (state, payload) => {
	state.userDel = payload;
};
export const hideUserAddForm = (state, payload) => {
	state.userAddTask = payload;
};
export const hideUserCreateForm = (state, payload) => {
	state.userCreate = payload;
};
export const hideUserChangePwdForm = (state, payload) => {
	state.userChangePwd = payload;
};
export const hideUserPrivPwdForm = (state, payload) => {
	state.userPriv = payload;
};
//******************************************************

// user form data **************************************
export const userName = (state, payload) => {
	state.userName = payload;
};
export const userEmail = (state, payload) => {
	state.userEmail = payload;
};
export const userAge = (state, payload) => {
	state.userAge = payload;
};
export const userBirthDate = (state, payload) => {
	state.userBirthDate = payload;
};
export const userId = (state, payload) => {
	state.userId = payload;
};
export const userLevel = (state, payload) => {
	state.userLevel = payload;
};
//******************************************************

// todo *************************************************
export const taskEdit = (state, payload) => {
	state.todoId = payload.todoId;
	state.todoNote = payload.todoNote;
	state.todoStatus = payload.todoStatus;
	state.taskEdit = payload.editForm;
};
export const taskDel = (state, payload) => {
	state.todoId = payload.todoId;
	state.todoName = payload.todoName;
	state.taskDel = payload.delForm;
};
export const taskAdd = (state, payload) => {
	state.taskAdd = payload;
};

// todo forms
export const hideEditForm = (state, payload) => {
	state.taskEdit = payload;
};
export const hideDelDialog = (state, payload) => {
	state.taskDel = payload;
};
export const hideAddDialog = (state, payload) => {
	state.taskAdd = payload;
};
// todo form data
export const taskNote = (state, payload) => {
	state.todoNote = payload;
};
export const todoStatus = (state, payload) => {
	state.todoStatus = payload;
};

//******************************************************

// user management privilege ***************************
export const userManagementPriv = (state, payload) => {
	state.umAccess = payload.umAccess;
	state.umModify = payload.umModify;
};
//******************************************************

// user information privilege ***************************
export const userInformationPriv = (state, payload) => {
	state.uiAccess = payload.uiAccess;
	state.uiAdd = payload.uiAdd;
	state.uiEdit = payload.uiEdit;
	state.uiDelete = payload.uiDelete;
};
//******************************************************

// todo privilege ***************************
export const todoPriv = (state, payload) => {
	state.tdAccess = payload.tdAccess;
	state.tdAdd = payload.tdAdd;
	state.tdEdit = payload.tdEdit;
	state.tdDelete = payload.tdDelete;
};
//******************************************************
